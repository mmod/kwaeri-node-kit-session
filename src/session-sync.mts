/**
 * SPDX-PackageName: kwaeri/session
 * SPDX-PackageVersion: 0.3.4
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as http from 'http';
import {
    ClientSession,
    SessionBits,
    SessionUserBits
} from './client-session.mjs';
import { BaseSessionStore } from '@kwaeri/session-store';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';


// DEFINES
const _     = new kdt(),
      DEBUG = debug( 'nodekit:session' );


export class SessionSync {
    private configuration;

    private ts?: any = null;

    private lut;

    public defaultCookiePath;

    public promoter;

    public store: any;


    /**
     * Class constructor
     *
     * @param { any } configuration
     *
     * @returns { constructor }
     */
    constructor( configuration: any ) {
        this.configuration = configuration;
        this.defaultCookiePath = '/';

        if( this.configuration.appType === 'admin' )
            this.defaultCookiePath = '/admin';

        DEBUG( `Set 'defaultCookiePath' to '${this.defaultCookiePath}'` );

        this.promoter = this.configuration.promoter;
        //delete this.config.promoter;

        // Configure the store:
        if( this.configuration.store && this.configuration.store !== "" )
            this.store = new this.configuration.store( _.get( this.configuration, 'session', {} ) );
        else
            this.store = new BaseSessionStore( _.get( this.configuration, 'session', {} ) );

        this.lut = [];
        for( var i=0; i<256; i++ ) {
            this.lut[i] = ( i < 16 ? '0' : '' )+( i ).toString( 16 );
        };
    }


    /**
     * Searches for a session
     *
     * @param { any } request
     * @param { any } response
     * @param { any } callback
     *
     * @return { void }
     */
    find( request: any, response: any, callback: any ): void {
        var cookies,
            cfg: SessionBits = {},
            ts = new Date(),    // Control
            stamp: Date|number = new Date(), // We need two Date objects since the first will act as a control - its required to instantiate 2 of them.
            cstamp,
            expired = false;

        // Start by parsing the cookies
        cookies = this.getCookies( request );
        cookies.id = cookies.id || this.getUniqueId();
        cookies.uk = cookies.uk || this.genId();

        DEBUG( `Get 'session' with 'id' [${cookies.id}]` );

        // Now check for the session by Id from the session store, and make sure the uk matches in order to further help in avoiding session take-over
        if( this.store.getSession( cookies.id ) && this.store.get( cookies.id, 'uk' ) === cookies.uk /*this.store.hasOwnProperty( cookies.id ) && this.store[cookies.id].uk === cookies.uk*/ ) {
            // Grab the current cookie expiration from the session store and then update the value to the new stamp.
            cstamp = new Date( this.store.get( cookies.id, 'expires' ) /*this.store[cookies.id].expires*/ );
            stamp = stamp.setTime( stamp.getTime() + ( 15 * 60 * 1000 ) );

            // If the session store's time stamp had expired, it just means the session cleanup cycle hasn't run, let's make sure to set
            // the authenticated value to false so that it is updated on the client's side.
            if( ts > cstamp )
                this.store.set( cookies.id, 'authenticated', false );

            //this.store[cookies.id].expires = stamp;
            this.store.set( cookies.id, 'expires', stamp );
        }
        else {
            // Otherwise create a new session using the cookie provided Id or new session Id that was generated.
            cfg.id = cookies.id;
            cfg.uk = cookies.uk;
            cfg.host = this.configuration.url;
            cfg.path = this.defaultCookiePath;
            cfg.expires = stamp;
            cfg.authenticated = false;
            cfg.user = { username: 'Guest', email: 'you@example.com', name: { first: 'Guest' } };
            if( this.configuration.domain ) { cfg.domain = this.configuration.domain; }
            //this.store[cookies.id] = new ClientSession( cfg );
            this.store.createSession( cookies.id, new ClientSession( cfg ) );

            DEBUG( `New 'session' with [id] '${cookies.id}' created` );
        }

        // We pass a copy of the client in the request, as well as the accepted cookies, and a method to fetch them
        request.session = {};
        request.session.client = this.store.getSession( cookies.id ); /*this.store[cookies.id];*/
        request.session.cookies = cookies;
        request.session.get = SessionSync.prototype.get;
        request.session.setUserAuth = this.moderate.bind( this );
        Object.defineProperty(
            request,
            'isAuthenticated',
            {
                value: this.isAuthenticated( cookies.id ),
                writable: false,
                enumerable: true,
                configurable: false
            }
        );

        DEBUG( `Set 'props' in 'request' (sid,cookies,Function)` );

        // We create an empty cookies member for allowing cookies to be set, then attach a method to do just that, as well as construct and send the headers
        response.session = {};
        response.session.tools = this.configuration.rtools;   // Add these tools for the set function;
        response.session.id = cookies.id;
        response.session.uk = cookies.uk;
        response.session.url = this.store.get( cookies.id, 'host' ); /* this.store[cookies.id].host;*/
        if( this.configuration.domain ) { response.session.domain = this.store.get( cookies.id, 'domain' ); /*this.store[cookies.id].domain;*/ }
        response.session.defaultCookiePath = this.defaultCookiePath;
        response.session.cookies = [];
        response.session.genExpirationDate = SessionSync.prototype.genExpirationDate;
        response.session.set = SessionSync.prototype.set;
        response.session.set( 'sid', JSON.stringify( { i: cookies.id, d: cookies.uk } ), { secure: true } );
        response.setSession = SessionSync.prototype.setSession;
        response.redirect = SessionSync.prototype.redirect;

        DEBUG( `Set 'props' in 'response' (cookies,Function,headers)` );

        callback( request, response );
    };



    /**
     * Parse cookies from the request
     *
     * @param { any } request
     *
     * @returns { any }
     */
    getCookies( request: any ): any {
        // We start by splitting the string by ';'...the last one doesn't need to have one :)
        let cstring,
            cookies: SessionBits = {};

        DEBUG( `Get 'cookies'` );

        if( request.headers.cookie ) {
            cstring = request.headers.cookie.toString();
            cstring = cstring.split( ';' );

            // We have an array of strings, each with an =
            for( let key in cstring ) {
                let i = cstring[key].split( '=' );
                if( i ) {
                    // Remove any leading spaces from the keys
                    i[0] = i[0].replace( ' ', '' );

                    // Now we have a single cookie key and value
                    if( i[0] == 'sid' ) {
                        let ids = JSON.parse( i[1] );
                        cookies.id = ids.i;
                        cookies.uk = ids.d;
                    }
                    else
                        ( cookies as any )[i[0]] = i[1];
                }
            }
        }

        return cookies;
    };


    /**
     * Generates a GUID.
     *
     * Based on the work of [Jeff Ward](http://stackoverflow.com/a/21963136), for the performance, and the saved time!
     *
     * @param { void }
     *
     * @returns { String }
     */
    genId(): string {
        const d0 = Math.random()*0xffffffff|0;
        const d1 = Math.random()*0xffffffff|0;
        const d2 = Math.random()*0xffffffff|0;
        const d3 = Math.random()*0xffffffff|0;

        return  this.lut[d0&0xff]+this.lut[d0>>8&0xff]+this.lut[d0>>16&0xff]+this.lut[d0>>24&0xff]+'-'+
                this.lut[d1&0xff]+this.lut[d1>>8&0xff]+'-'+this.lut[d1>>16&0x0f|0x40]+this.lut[d1>>24&0xff]+'-'+
                this.lut[d2&0x3f|0x80]+this.lut[d2>>8&0xff]+'-'+this.lut[d2>>16&0xff]+this.lut[d2>>24&0xff]+
                this.lut[d3&0xff]+this.lut[d3>>8&0xff]+this.lut[d3>>16&0xff]+this.lut[d3>>24&0xff];
    };


    /**
     * Gets a unique ID for a new client session
     *
     * @param { void }
     *
     * @returns { string } A unique id
     */
    getUniqueId(): string {
        let id = this.genId();

        while( this.store.getSession( id ) ) {
            id = this.genId();
        }

        return id;
    }


    /**
     * Moderates the session from the controllerC
     * @param { any } unauth
     *
     * @returns { void }
     */
    moderate( sid: any, user: any, unauth: any ): void {
        if( !unauth ) {
            //this.store[sid].authenticated = true;
            this.store.set( sid, 'authenticated', true );
            //this.store[sid].user = user;
            this.store.set( sid, 'user', user );

            DEBUG( `${sid} is now authenticated: ${this.store.get( sid, "authenticated" )}` );
        }
        else {
            this.store.deleteSession( sid );

            DEBUG( `${sid} is now signed out.` );
        }
    };


    /**
     * Checks if client is authenticated
     *
     * @param { string } sid
     *
     * @returns { boolean }
     */
    isAuthenticated( sid: string ): boolean {
        DEBUG( `Get 'authenticated' for '${sid}'` );

        return this.store.get( sid, 'authenticated' ); /*this.store[sid].authenticated;*/
    };


    /**
     * Generates a cookie expiration date string
     *
     * @param { number } minutes  Number of minutes the cookie should persist
     *
     * @returns { string }
     */
    genExpirationDate( minutes: number ): string {
        var d = new Date();
        d.setTime( d.getTime() + ( minutes * 60 * 1000 ) );

        DEBUG( `Set 'expires' to '${d.toUTCString()}'` );

        return d.toUTCString();
    };


    /**
     * Starts the session service. The session runs cleanup ever 5 minutes (default)
     *
     * @param { number } minutes The number of minutes that should span between session cleanup
     *
     * @return { void }
     */
    start( minutes: number = 5 ): void {
        // For now we will default the timer to every 5 minutes.
        this.ts = new Date();
        //timestamp = new Date();

        this.cleanup( ( minutes*60*1000 ) );
    };


    /**
     * Sets a Data/session variable
     *
     * @param { string } name
     * @param { any } value
     * @param { any } options
     *
     * @return { boolean }
     */
    set( name: string, value: any, options: any ): boolean {
        let def = {
            host: ( this as any ).url,
            domain: ( this as any ).domain,
            path: this.defaultCookiePath,
            secure: false,
            expires: this.genExpirationDate( 30 )
        },
        o = _.extend( options || {}, def );

        if( ( o as any ).secure )
            ( o as any ).secure = 'HttpOnly';

        if( name && value ) {
            if( name === 'user' )
                value = JSON.stringify( value );

            ( this as any ).cookies.push(`${name}=${value}; ${((( o as any ).domain)?`domain=${( o as any ).domain}; `:`host=${( o as any ).host}; `)}path=${( o as any ).path}; expires=${( o as any ).expires}; ${( o as any ).secure}` );

            DEBUG(`Set 'cookie' [${name}] to '${value} with ${((( o as any ).domain)?`domain=${( o as any ).domain}; `:`host=${( o as any ).host}; `)} path='${( o as any ).path}'; expires='${( o as any ).expires}'; ${( o as any ).secure}`);

            return true;
        }

        DEBUG( `Call 'set' [cookie] with '${name}'` );

        return false;
    };


    /**
     * Gets a Data/session variable
     *
     * @param { string } name
     * @param { any } def
     *
     * @returns { any }
     */
    get( name: string, def: any ): any {
        if( name ) {
            DEBUG( `Call 'get' [cookie] with '${name}'` );

            if( ( this as any ).cookies.hasOwnProperty( name ) ) {
                // Add logic for supporting proprietary user cookie
                if( name === 'user' )
                    return JSON.parse( ( this as any ).cookies[name] );
                else
                    return ( this as any ).cookies[name];
            }
            else
                if( def )
                    return def;
                else // Let's set the default user cookie if it was requested, but not set, and defaults not provided.
                    if( name === 'user' )
                        return { username: 'Guest', email: 'you@example.com', name: { first: 'Guest' } };
                    else
                        return {};
        }
    };


    /**
     * Sets the sessions cookies
     *
     * @param { void }
     *
     * @returns { void }
     */
    setSession(): void {
        // setHeader is a member of the response object:
        ( this as any ).setHeader( 'Set-Cookie', ( this as any ).session.cookies );
    };


    /**
     * Sends a redirect header to the client
     *
     * @param { string } path
     *
     * @returns { void }
     */
    redirect( path: string ): void {
        DEBUG( `Execute client redirect` );

        // Set the response status:
        ( this as unknown as http.ServerResponse ).statusCode = 302;

        DEBUG( `Set 'status' [HTTP] to '302'` );

        // Set the cookie headers
        this.setSession();

        // Set the location header:
        ( this as unknown as http.ServerResponse ).setHeader( 'Location', path );

        DEBUG( `Set 'location' to '${path}'` );

        // Write the
        ( this as unknown as http.ServerResponse ).end( '<p>Please wait while we redirect you.</p>' );
    };


    /**
     * Performs cleanup tasks
     *
     * @param { number } delay The number of minutes until the next cleanup
     *
     * @returns { void }
     */
    cleanup( delay: number ): void {
        var reaper = this,
            count = 0,
            ts = new Date();

        DEBUG( `Session clean up started` );

        this.store.cleanSessions();

        /*
            Calculating difference in time:  Based strongly on:
            https://stackoverflow.com/a/13904120
        */

        // Get total seconds between the times:
        let delta = Math.abs( ts.getTime() - this.ts.getTime() ) / 1000;

        // Calculate (and subtract) whole days
        const days = Math.floor( delta / 86400 );
        delta -= days * 86400;

        // Calculate (and subtract) whole hours
        const hours = Math.floor( delta / 3600 ) % 24;
        delta -= hours * 3600;

        // Calculate (and subtract) whole minutes
        const minutes = Math.floor( delta / 60 ) % 60;
        delta -= minutes * 60;

        // What's left is seconds
        const seconds = Math.round( delta % 60 );

        // Round those seconds up to the nearest whole number!
        //seconds = Math.round( seconds );

        console.log( `${count} active client sessions. Up since ${this.ts} (${(( days ) ? `${days} days ` : '' )}${(( hours ) ? `${hours} hours ` : ``)}${(( minutes ) ? `${minutes} minutes ` : ``)}${(( seconds ) ? `${seconds} seconds ` : ``)})` );

        setTimeout( () => { reaper.cleanup( delay ); }, delay );
    };
}

