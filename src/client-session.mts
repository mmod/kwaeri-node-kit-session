/**
 * SPDX-PackageName: kwaeri/session
 * SPDX-PackageVersion: 0.3.4
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


export type SessionBits = {
    id?: string,
    uk?: string,
    host?: string,
    domain?: string,
    path?: string,
    expires?: Date|number,
    authenticated?: boolean,
    user?: SessionUserBits
};

export type SessionUserBits = {
    username: string,
    email: string,
    name: {
        first: string,
        last?: string
    }
};

export class ClientSession {
    private id;
    private uk;
    private host;
    private domain;
    private path;
    private persistent;
    private expires;
    private authenticated;
    private user;

    constructor( configuration: any ) {
        this.id             = configuration.id;
        this.uk             = configuration.uk;
        this.host           = configuration.host;
        if( configuration.domain ) { this.domain = configuration.domain; }
        this.path           = configuration.path || '/';
        this.persistent     = configuration.persistent || true;
        this.expires        = configuration.expires;
        this.authenticated  = configuration.authenticated;
        this.user           = configuration.user;
    }
}